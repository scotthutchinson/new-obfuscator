<?php

class Token
{
    private $value;
    private $index;
    private $type;


    public function __construct($value, $index, $type = null)
    {
        $this->value = $value;
        $this->index = $index;
        $this->type = $type;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function setIndex($index)
    {
        $this->index = $index;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}
