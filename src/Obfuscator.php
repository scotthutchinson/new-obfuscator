<?php

require_once 'Pattern.php';
require_once 'Lexer.php';
require_once 'Parser.php';


class Obfuscator
{
    private $lexer;
    private $parser;

    public function init()
    {
        $this->lexer = new Lexer;

        $sourceTokens = $this->lexer->initTokens('in/test_src_good_orig.php');
        // $sourceTokens = $this->lexer->initTokens('in/test_src_global.php');


        echo '<pre>';

        $this->parser = new Parser;
        $this->output = $this->parser->init($sourceTokens);

        var_dump($this->parser->getFunctionUsageNames());


        $staticPrefixes = range('a', 'l');
        $prefixes = range('m', 'z');
        $suffixes = range(0, 99);

        $obfuscatedConstantNames = $this->getObfuscatedConstantNames($prefixes, $suffixes);
        $obfuscatedStaticMemberNames = $this->getObfuscatedStaticMembersNames($staticPrefixes, $suffixes);
        $obfuscatedMemberNames = $this->getObfuscatedMembersNames($prefixes, $suffixes);
        $obfuscatedStaticFunctionNames = $this->getObfuscatedFunctionNames($staticPrefixes, $suffixes);
        $obfuscatedFunctionNames = $this->getObfuscatedFunctionNames($prefixes, $suffixes);

        $constantReplace = $this->getConstantReplace($this->parser->getConstantNames(), $obfuscatedConstantNames);

        $staticMemberReplace = $this->getStaticMemberReplace($this->parser->getStaticMemberNames(), $obfuscatedStaticMemberNames);

        $memberDefinitionReplace = $this->getMemberDefinitionReplace($this->parser->getMemberNames(), $obfuscatedMemberNames);
        $memberUsageReplace = $this->getMemberUsageReplace($this->parser->getMemberNames(), $obfuscatedMemberNames);

        $staticFunctionReplace = $this->getFunctionReplace($this->parser->getStaticFunctionNames(), $obfuscatedStaticFunctionNames);
        $functionReplace = $this->getFunctionReplace($this->parser->getFunctionNames(), $obfuscatedFunctionNames);
        var_dump($functionReplace);


        $updatedTokens = array_merge(
            $this->replaceEntity($this->parser->getConstantDefinitionNames(), $constantReplace),
            $this->replaceEntity($this->parser->getConstantUsageNames(), $constantReplace),
            $this->replaceEntity($this->parser->getStaticMemberDefinitionNames(), $staticMemberReplace),
            $this->replaceEntity($this->parser->getStaticMemberUsageNames(), $staticMemberReplace),
            $this->replaceEntity($this->parser->getMemberDefinitionNames(), $memberDefinitionReplace),
            $this->replaceEntity($this->parser->getMemberUsageNames(), $memberUsageReplace),
            $this->replaceEntity($this->parser->getFunctionDefinitionNames(), $functionReplace),
            $this->replaceEntity($this->parser->getFunctionUsageNames(), $functionReplace),
            $this->replaceEntity($this->parser->getStaticFunctionDefinitionNames(), $staticFunctionReplace),
            $this->replaceEntity($this->parser->getStaticFunctionUsageNames(), $staticFunctionReplace)
        );

        $this->replaceTokens($sourceTokens, $updatedTokens);

        return $this->toString($sourceTokens);
    }

    private function replaceEntity($tokens, $replace)
    {
        foreach ($tokens as $token) {
            foreach ($replace as $findValue => $replaceValue) {
                if ($token->getValue() === $findValue) {
                    $token->setValue($replaceValue);
                }
            }
        }

        return $tokens;
    }

    private function replaceTokens($sourceTokens, $updatedTokens)
    {
        foreach ($updatedTokens as $token) {
            $sourceTokens[$token->getIndex()] = $token;
        }
    }

    private function toString($tokens)
    {
        $output = '';

        foreach ($tokens as $token) {
            $output .= $token->getValue();
        }

        return $output;
    }

    private function getObfuscatedConstantNames($prefixes, $suffixes)
    {
        $functions = array();
        foreach ($prefixes as $prefix) {
            foreach ($suffixes as $suffix) {
                $functions[] = $prefix . $suffix;
            }
        }

        return $functions;
    }

    private function getObfuscatedStaticMembersNames($prefixes, $suffixes)
    {
        $variables = array();
        foreach ($prefixes as $prefix) {
            foreach ($suffixes as $suffix) {
                $variables[] = '$' . $prefix . $suffix;
            }
        }

        return $variables;
    }

    private function getObfuscatedMembersNames($prefixes, $suffixes)
    {
        $variables = array();
        foreach ($prefixes as $prefix) {
            foreach ($suffixes as $suffix) {
                $variables[] = '$' . $prefix . $suffix;
            }
        }

        return $variables;
    }

    private function getObfuscatedFunctionNames($prefixes, $suffixes)
    {
        $functions = array();
        foreach ($prefixes as $prefix) {
            foreach ($suffixes as $suffix) {
                $functions[] = $prefix . $suffix;
            }
        }

        return $functions;
    }

    private function getStaticMemberReplace($memberVariables, $variables)
    {
        return $this->getReplaceNames($memberVariables, $variables,
                                      'Parser::addVariableSymbol');
    }

    private function getMemberDefinitionReplace($memberVariables, $variables)
    {
        return $this->getReplaceNames($memberVariables, $variables,
                                      'Parser::addVariableSymbol');
    }

    private function getConstantReplace($functionNames, $functions)
    {
        return $this->getReplaceNames($functionNames, $functions);
    }

    private function getMemberUsageReplace($memberVariables, $variables)
    {
        return $this->getReplaceNames($memberVariables, $variables,
                                      null, 'Parser::removeVariableSymbol');
    }

    private function getFunctionReplace($functionNames, $functions)
    {
        return $this->getReplaceNames($functionNames, $functions);
    }

    private function getReplaceNames($names, $obfuscatedNames,
                                     $nameFilterCallback = null,
                                     $obfuscatedNameFilterCallback = null)
    {
        $replace = array();
        $index = 0;
        foreach ($names as $name) {
            if ($nameFilterCallback !== null) {
                $name = call_user_func($nameFilterCallback, $name);
            }

            $obfuscatedName = $obfuscatedNames[$index];
            if ($obfuscatedNameFilterCallback !== null) {
                $obfuscatedName = call_user_func($obfuscatedNameFilterCallback,
                                                 $obfuscatedNames[$index]);
            }

            if ($obfuscatedName !== null) {
                $replace[$name] = $obfuscatedName;
                ++$index;
            }
        }

        return $replace;
    }
}
