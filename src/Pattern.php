<?php

class Pattern
{
    private $valueIndex;
    private $components;

    public function __construct($valueIndex, $components)
    {
        $this->valueIndex = $valueIndex;
        $this->components = $components;
    }

    public function getValueIndex()
    {
        return $this->valueIndex;
    }

    public function getComponents()
    {
        return $this->components;
    }

    public function setValueIndex($valueIndex)
    {
        $this->valueIndex = $valueIndex;
    }

    public function setComponents($components)
    {
        $this->components = $components;
    }
}
