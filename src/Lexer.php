<?php

require_once 'Token.php';

class Lexer
{
    public function initTokens($filename)
    {
        $sourceTokens = array();

        $fileContent = file_get_contents($filename);
        // $fileContent = php_strip_whitespace($filename);
        $rawTokens = token_get_all($fileContent);

        $tokenIndex = 0;
        foreach ($rawTokens as $rawToken) {
            if (is_array($rawToken)) {
                $sourceTokens[] = new Token($rawToken[1], $tokenIndex, $rawToken[0]);
            }
            else {
                $sourceTokens[] = new Token($rawToken, $tokenIndex);
            }

            $tokenIndex++;
        }

        return $sourceTokens;
    }
}
