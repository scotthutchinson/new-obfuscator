<?php

class Parser
{
    private $classDefinitionNames;

    private $constantDefinitionNames;
    private $staticMemberDefinitionNames;
    private $memberDefinitionNames;
    private $staticFunctionDefinitionNames;
    private $functionDefinitionNames;

    private $constantUsageNames;
    private $staticMemberUsageNames;
    private $memberUsageNames;
    private $staticFunctionUsageNames;
    private $functionUsageNames;
    private $constructorUsageNames;

    private $constantNames;
    private $staticMemberNames;
    private $memberNames;
    private $staticFunctionNames;
    private $functionNames;

    private static $ignoredConstructorUsageNames = array(
        'static',
        'self',
    );

    private static $ignoredFunctionDefinitionNames = array(
        '__construct',
        '__destruct',
        '__call',
        '__callStatic',
        '__get',
        '__set',
        '__isset',
        '__unset',
        '__sleep',
        '__wakeup',
        '__toString',
        '__invoke',
        '__set_state',
        '__clone',
        '__debugInfo',
    );

    public function init($sourceTokens)
    {
        $patternClassDefinition = new Pattern(2, array(
            'class',
            ' ',
            '^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$',
        ));

        $patternStaticMemberUsage = new Pattern(2, array(
            '(self|static|parent|^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$)',
            '::',
            '\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
        ));

        $patternMemberUsage = new Pattern(2, array(
            '\$this',
            '->',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '[^(]+',
        ));

        $patternStaticFunctionUsage = new Pattern(2, array(
            '(self|static|parent|^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$)',
            '::',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '\(',
        ));

        $patternFunctionUsage = new Pattern(2, array(
            '(\$this|\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)',
            '->',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '\(',
        ));

        $patternStaticMemberDefinition = new Pattern(4, array(
            '(public|private|protected)',
            ' ',
            'static',
            ' ',
            '\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '[^(]+',
        ));

        $patternMemberDefinition = new Pattern(2, array(
            '(public|private|protected)',
            ' ',
            '\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '[^(]+',
        ));

        $patternStaticFunctionDefinition = new Pattern(6, array(
            '(public|private|protected)',
            ' ',
            'static',
            ' ',
            'function',
            ' ',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '\(+',
        ));

        $patternFunctionDefinition = new Pattern(4, array(
            '(public|private|protected|abstract)',
            ' ',
            'function',
            ' ',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '\(+',
        ));

        $patternConstantDefinition = new Pattern(2, array(
            'const',
            ' ',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
        ));

        $patternConstantUsage = new Pattern(2, array(
            '(self|static|parent)',
            '::',
            '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*',
            '[^(]+',
        ));

        $patternConstructorUsage = new Pattern(2, array(
            'new',
            ' ',
            '^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$',
        ));


        $this->classDefinitionNames = $this->getValueTokens(
            $sourceTokens,
            $patternClassDefinition);

        $this->constantUsageNames = $this->getValueTokens(
            $sourceTokens,
            $patternConstantUsage);

        $this->staticMemberUsageNames = $this->getValueTokens(
            $sourceTokens,
            $patternStaticMemberUsage);

        $this->memberUsageNames = $this->getValueTokens(
            $sourceTokens,
            $patternMemberUsage);

        $this->staticFunctionUsageNames = $this->getValueTokens(
            $sourceTokens,
            $patternStaticFunctionUsage);

        $this->functionUsageNames = $this->getValueTokens(
            $sourceTokens,
            $patternFunctionUsage);

        $this->constructorUsageNames = $this->filterTokens(
            $this->getValueTokens($sourceTokens, $patternConstructorUsage),
            Parser::$ignoredConstructorUsageNames);

        $this->constantDefinitionNames = $this->getValueTokens(
            $sourceTokens,
            $patternConstantDefinition);

        $this->staticMemberDefinitionNames = $this->getValueTokens(
            $sourceTokens,
            $patternStaticMemberDefinition);

        $this->memberDefinitionNames = $this->getValueTokens(
            $sourceTokens,
            $patternMemberDefinition);

        $this->staticFunctionDefinitionNames = $this->getValueTokens(
            $sourceTokens,
            $patternStaticFunctionDefinition);

        $this->functionDefinitionNames = $this->filterTokens(
            $this->getValueTokens($sourceTokens, $patternFunctionDefinition),
            Parser::$ignoredFunctionDefinitionNames);

        $this->constantNames = $this->initConstantNames(
            $this->constantDefinitionNames,
            $this->constantUsageNames);

        $this->staticMemberNames = $this->initMemberNames(
            $this->staticMemberDefinitionNames,
            $this->staticMemberUsageNames);

        $this->memberNames = $this->initMemberNames(
            $this->memberDefinitionNames,
            $this->memberUsageNames);

        $this->staticFunctionNames = $this->initFunctionNames(
            $this->staticFunctionDefinitionNames,
            $this->staticFunctionUsageNames);

        $this->functionNames = $this->initFunctionNames(
            $this->functionDefinitionNames,
            $this->functionUsageNames);
    }

    private function getValueTokens($sourceTokens, $pattern)
    {
        $names = array();

        $valueIndex = $pattern->getValueIndex();
        $matches = $this->matchPattern($sourceTokens, $pattern);
        foreach ($matches as $match) {
            if (isset($match[$valueIndex])) {
                $names[] = $match[$valueIndex];
            }
        }

        return $names;
    }

    private function matchPattern($tokens, $pattern)
    {
        $patternMatches = array();
        $patternIndex = 0;

        $temp = array();

        $components = $pattern->getComponents();

        foreach ($tokens as $token) {
            if (isset($components[$patternIndex])) {
                $regex = '/' . $components[$patternIndex] . '/';
                preg_match($regex, $token, $matches);
                if (!empty($matches[0])) {
                    ++$patternIndex;
                    $temp[] = $token;

                    if ($patternIndex === count($components)) {
                        $patternIndex = 0;
                        $patternMatches[] = $temp;
                        $temp = array();
                    }
                }
                else {
                    $temp = array();
                    $patternIndex = 0;
                }
            }
        }

        return $patternMatches;
    }

    private function getUniqueNames($definitionNames, $usageNames)
    {
        $names1 = array();
        foreach ($definitionNames as $definitionName) {
            $names1[] = $definitionName->getValue();
        }
        $names1 = array_unique($names1);

        $names2 = array();
        foreach ($usageNames as $usageName) {
            $names2[] = $usageName->getValue();
        }
        $names2 = array_unique($names2);

        return array_values($names1 + $names2);
    }

    private function filterTokens($tokens, $blackList)
    {
        foreach ($tokens as $index => $token) {
            if (in_array($token->getValue(), $blackList)) {
                unset($tokens[$index]);
            }
        }

        return array_values($tokens);
    }

    private function initConstantNames($memberDefinitionNames,
                                       $memberUsageNames)
    {
        return $this->getUniqueNames($memberDefinitionNames, $memberUsageNames);
    }

    private function initMemberNames($memberDefinitionNames, $memberUsageNames)
    {
        $uniqueNames = $this->getUniqueNames($memberDefinitionNames, $memberUsageNames);
        $uniqueNames = array_map('Parser::removeVariableSymbol', $uniqueNames);

        return $uniqueNames;
    }

    private function initFunctionNames($functionDefinitionNames,
                                       $functionUsageNames)
    {
        return $this->getUniqueNames($functionDefinitionNames, $functionUsageNames);
    }

    public static function addVariableSymbol($tokenName)
    {
        return '$' . $tokenName;
    }

    public static function removeVariableSymbol($tokenName)
    {
        return substr($tokenName, 1);
    }



    public function getConstantDefinitionNames()
    {
        return $this->constantDefinitionNames;
    }

    public function getStaticMemberDefinitionNames()
    {
        return $this->staticMemberDefinitionNames;
    }

    public function getMemberDefinitionNames()
    {
        return $this->memberDefinitionNames;
    }

    public function getStaticFunctionDefinitionNames()
    {
        return $this->staticFunctionDefinitionNames;
    }

    public function getFunctionDefinitionNames()
    {
        return $this->functionDefinitionNames;
    }



    public function getConstantUsageNames()
    {
        return $this->constantUsageNames;
    }

    public function getStaticMemberUsageNames()
    {
        return $this->staticMemberUsageNames;
    }

    public function getMemberUsageNames()
    {
        return $this->memberUsageNames;
    }

    public function getStaticFunctionUsageNames()
    {
        return $this->staticFunctionUsageNames;
    }

    public function getFunctionUsageNames()
    {
        return $this->functionUsageNames;
    }



    public function getConstantNames()
    {
        return $this->constantNames;
    }

    public function getStaticMemberNames()
    {
        return $this->staticMemberNames;
    }

    public function getMemberNames()
    {
        return $this->memberNames;
    }

    public function getStaticFunctionNames()
    {
        return $this->staticFunctionNames;
    }

    public function getFunctionNames()
    {
        return $this->functionNames;
    }
}
